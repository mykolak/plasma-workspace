# Frederik Schwarzer <schwarzer@kde.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-03 00:46+0000\n"
"PO-Revision-Date: 2014-06-28 18:00+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: kdedksysguard.cpp:40
#, kde-format
msgid "Show System Activity"
msgstr "Systemaktivität anzeigen"

#: main.cpp:23
#, kde-format
msgid "System Activity"
msgstr "Systemaktivität"
