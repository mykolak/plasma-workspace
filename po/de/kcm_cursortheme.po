# Burkhard Lück <lueck@hube-lueck.de>, 2015, 2018, 2019, 2021.
# Frederik Schwarzer <schwarzer@kde.org>, 2016, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2021-07-01 09:34+0200\n"
"Last-Translator: Burkhard Lück <lueck@hube-lueck.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: label, entry (cursorTheme), group (Mouse)
#: cursorthemesettings.kcfg:9
#, kde-format
msgid "Name of the current cursor theme"
msgstr "Name des aktuellen Mauszeigerdesigns"

#. i18n: ectx: label, entry (cursorSize), group (Mouse)
#: cursorthemesettings.kcfg:13
#, kde-format
msgid "Current cursor size"
msgstr "Aktuelle Zeigergröße"

#: kcmcursortheme.cpp:302
#, kde-format
msgid ""
"You have to restart the Plasma session for these changes to take effect."
msgstr ""
"Die Plasma-Sitzung muss neu gestartet werden, damit die Änderungen wirksam "
"werden."

#: kcmcursortheme.cpp:376
#, kde-format
msgid "Unable to create a temporary file."
msgstr "Es kann keine temporäre Datei erstellt werden."

#: kcmcursortheme.cpp:387
#, kde-format
msgid "Unable to download the icon theme archive: %1"
msgstr "Das Archiv für das Symboldesign %1 kann nicht heruntergeladen werden."

#: kcmcursortheme.cpp:418
#, kde-format
msgid "The file is not a valid icon theme archive."
msgstr "Die Datei ist kein gültiges Symboldesign-Archiv."

#: kcmcursortheme.cpp:425
#, kde-format
msgid "Failed to create 'icons' folder."
msgstr "Der Ordner „Symbole“ lässt sich nicht erzeugen."

#: kcmcursortheme.cpp:434
#, kde-format
msgid ""
"A theme named %1 already exists in your icon theme folder. Do you want "
"replace it with this one?"
msgstr ""
"Ein Design namens %1 existiert bereits im Ordner für Symboldesigns. Möchten "
"Sie es überschreiben?"

#: kcmcursortheme.cpp:438
#, kde-format
msgid "Overwrite Theme?"
msgstr "Design überschreiben?"

#: kcmcursortheme.cpp:462
#, kde-format
msgid "Theme installed successfully."
msgstr "Das Design wurde erfolgreich installiert."

#: package/contents/ui/Delegate.qml:54
#, kde-format
msgid "Remove Theme"
msgstr "Design entfernen"

#: package/contents/ui/Delegate.qml:61
#, kde-format
msgid "Restore Cursor Theme"
msgstr "Mauszeigerdesign wiederherstellen"

#: package/contents/ui/main.qml:20
#, kde-format
msgid "This module lets you choose the mouse cursor theme."
msgstr "Mit diesem Modul können Sie das Design für den Mauszeiger auswählen."

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Size:"
msgstr "Größe:"

#: package/contents/ui/main.qml:134
#, kde-format
msgid "&Install from File…"
msgstr "&Aus Datei installieren ..."

#: package/contents/ui/main.qml:140
#, kde-format
msgid "&Get New Cursors…"
msgstr "&Neue Zeiger holen ..."

#: package/contents/ui/main.qml:157
#, kde-format
msgid "Open Theme"
msgstr "Design öffnen"

#: package/contents/ui/main.qml:159
#, kde-format
msgid "Cursor Theme Files (*.tar.gz *.tar.bz2)"
msgstr "Zeigerdesigndateien (*.tar.gz *.tar.bz2)"

#: plasma-apply-cursortheme.cpp:42
#, kde-format
msgid "The requested size '%1' is not available, using %2 instead."
msgstr ""

#: plasma-apply-cursortheme.cpp:63
#, kde-format
msgid ""
"This tool allows you to set the mouse cursor theme for the current Plasma "
"session, without accidentally setting it to one that is either not "
"available, or which is already set."
msgstr ""

#: plasma-apply-cursortheme.cpp:67
#, kde-format
msgid ""
"The name of the cursor theme you wish to set for your current Plasma session "
"(passing a full path will only use the last part of the path)"
msgstr ""

#: plasma-apply-cursortheme.cpp:68
#, kde-format
msgid ""
"Show all the themes available on the system (and which is the current theme)"
msgstr ""

#: plasma-apply-cursortheme.cpp:69
#, kde-format
msgid "Use a specific size, rather than the theme default size"
msgstr ""

#: plasma-apply-cursortheme.cpp:90
#, kde-format
msgid ""
"The requested theme \"%1\" is already set as the theme for the current "
"Plasma session."
msgstr ""

#: plasma-apply-cursortheme.cpp:101
#, kde-format
msgid ""
"Successfully applied the mouse cursor theme %1 to your current Plasma session"
msgstr ""

#: plasma-apply-cursortheme.cpp:103
#, fuzzy, kde-format
#| msgid ""
#| "You have to restart the Plasma session for these changes to take effect."
msgid ""
"You have to restart the Plasma session for your newly applied mouse cursor "
"theme to display correctly."
msgstr ""
"Die Plasma-Sitzung muss neu gestartet werden, damit die Änderungen wirksam "
"werden."

#: plasma-apply-cursortheme.cpp:113
#, kde-format
msgid ""
"Could not find theme \"%1\". The theme should be one of the following "
"options: %2"
msgstr ""

#: plasma-apply-cursortheme.cpp:121
#, kde-format
msgid "You have the following mouse cursor themes on your system:"
msgstr "Sie haben die folgenden Mauszeiger-Designs auf Ihrem System:"

#: plasma-apply-cursortheme.cpp:126
#, kde-format
msgid "(Current theme for this Plasma session)"
msgstr ""

#: xcursor/xcursortheme.cpp:60
#, kde-format
msgctxt ""
"@info The argument is the list of available sizes (in pixel). Example: "
"'Available sizes: 24' or 'Available sizes: 24, 36, 48'"
msgid "(Available sizes: %1)"
msgstr "(Verfügbare Größen: %1)"

#~ msgid "Name"
#~ msgstr "Name"

#~ msgid "Description"
#~ msgstr "Beschreibung"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Thomas Diehl"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "thd@kde.org"

#~ msgid "Cursors"
#~ msgstr "Zeiger"

#~ msgid "(c) 2003-2007 Fredrik Höglund"
#~ msgstr "(c) 2003-2007 Fredrik Höglund"

#~ msgid "Fredrik Höglund"
#~ msgstr "Fredrik Höglund"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid ""
#~ "<qt>You cannot delete the theme you are currently using.<br />You have to "
#~ "switch to another theme first.</qt>"
#~ msgstr ""
#~ "<qt>Das derzeit verwendete Zeigerdesign kann nicht entfernt werden.<br /"
#~ ">Wechseln Sie zuerst das aktuelle Zeigerdesign und versuchen es dann "
#~ "erneut.</qt>"

#~ msgid ""
#~ "<qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This "
#~ "will delete all the files installed by this theme.</qt>"
#~ msgstr ""
#~ "<qt>Sind Sie sicher, dass Sie das Zeigerdesign <i>%1</i> entfernen "
#~ "möchten?<br />Dadurch werden alle Dateien gelöscht, die von dem Design "
#~ "installiert wurden.</qt>"

#~ msgid "Confirmation"
#~ msgstr "Bestätigung"

#~ msgctxt "@item:inlistbox size"
#~ msgid "Resolution dependent"
#~ msgstr "Auflösungsabhängig"

#~ msgid "Cursor Settings Changed"
#~ msgstr "Zeiger-Einstellungen geändert"

#~ msgid "Drag or Type Theme URL"
#~ msgstr "Geben Sie eine Design-Adresse ein oder ziehen Sie sie herein"

#~ msgid ""
#~ "Unable to download the cursor theme archive; please check that the "
#~ "address %1 is correct."
#~ msgstr ""
#~ "Herunterladen der Archivdatei für Zeigerdesign nicht möglich. Überprüfen "
#~ "Sie bitte, ob die Adresse %1 korrekt ist."

#~ msgid "Get new color schemes from the Internet"
#~ msgstr "Neue Farbschemata aus dem Internet herunterladen"

#~ msgid ""
#~ "Select the cursor theme you want to use (hover preview to test cursor):"
#~ msgstr ""
#~ "Bitte wählen Sie das zu verwendende Zeigerdesign\n"
#~ "Um den Zeiger zu testen, bewegen Sie ihn über den Vorschaubereich:"
